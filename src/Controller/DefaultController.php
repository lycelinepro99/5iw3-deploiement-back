<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
class DefaultController extends AbstractController
{
    public function index(): JsonResponse
    {
        // return new JsonResponse([
        //     [
        //         'id' => 1,
        //         'title' => 'test 1',
        //         'completed' => false,
        //     ],
        // ], 200, ['Access-Control-Allow-Origin' => '*']);
        $data = $this->getDoctrine()->getRepository(Task::class)->findAll();
        // var_dump($data);
        $arr = [];
        foreach ($data as $item) {
            $itemJson = [
                'id' => $item->getId(),
                'title' => $item->getTitle(),
                'completed' => $item->getCompleted(),
            ];
            array_push($arr, $itemJson);
        }
        return new JsonResponse($arr, 200, ['Access-Control-Allow-Origin' => '*']);
    }
}

