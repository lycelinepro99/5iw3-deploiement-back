<?php

namespace App\Controller;

use App\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Exception;


/**
 *
 * @Route("/task", name="task_")
 */
class TaskController extends AbstractController
{
    /**
     *
     * @Route("/", name="task_read", methods={"GET"})
     *
     */
    public function getTask(): JsonResponse
    {
        $data = $this->getDoctrine()->getRepository(Task::class)->findAll();
        // var_dump($data);
        $arr = [];
        foreach ($data as $item) {
            $itemJson = [
                'id' => $item->getId(),
                'title' => $item->getTitle(),
                'completed' => $item->getCompleted(),
            ];
            array_push($arr, $itemJson);
        }
        return new JsonResponse($arr, 200, ['Access-Control-Allow-Origin' => '*']);
    }
    /**
     * 
     * @Route("/create", name="task_create", methods={"POST"})
     * 
     * @return Response
     */
    public function createTask(Request $request)
    {
        $task = new Task();
        $title = $request->query->get('title');
        $completed = $request->query->get('completed');

        $task->setTitle($title);
        $task->setCompleted($completed);
        try {
            $em = $this->getDoctrine()->getManager();

            $em->persist($task);
            $em->flush();
            return $this->JsonResponse($this->view(['status' => 'ok', 'id' => "", $task->getId()], Response::HTTP_CREATED));
        } catch (Exception $e) {
            return $this->JsonResponse($this->view(['error' => $e->getMessage()], Response::HTTP_ACCEPTED));
        }
    }
}
